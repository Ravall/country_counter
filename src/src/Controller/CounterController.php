<?php

declare(strict_types=1);

namespace App\Controller;

use App\CountryCounter\CounterCalculate;
use App\CountryCounter\UseCase\IncreaseCountryCount;
use App\Http\JsonApiResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

final class CounterController
{
    #[Route('/countries/{countryCode}/increase', name: 'increase', methods: ["POST"])]
    public function increase(string $countryCode, MessageBusInterface $messageBus): JsonResponse
    {
        $messageBus->dispatch(new IncreaseCountryCount($countryCode));
        return JsonApiResponse::success();
    }

    #[Route('/countries/stat', name: 'report', methods: ["GET"])]
    public function report(CounterCalculate $counterCalculate): JsonResponse
    {
        return JsonApiResponse::success(
            CountryCodeBag::fromCountryCounts($counterCalculate->getAllCountries())
        );
    }
}