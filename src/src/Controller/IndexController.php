<?php

declare(strict_types=1);

namespace App\Controller;

use App\Http\JsonApiResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class IndexController
{
    #[Route('/', name: 'home', methods: ["GET"])]
    public function report(): JsonResponse
    {
        return JsonApiResponse::success();
    }
}