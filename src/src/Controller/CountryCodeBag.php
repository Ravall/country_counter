<?php

declare(strict_types=1);

namespace App\Controller;

use App\Http\Type;

final class CountryCodeBag implements Type
{
    private function __construct(
        public array $data
    ) {}

    /**
     * @param array<string, int> $data
     * @return CountryCodeBag
     */
    public static function fromCountryCounts(array $data): CountryCodeBag
    {
        return new CountryCodeBag($data);
    }

    public function name(): string
    {
        return 'statistic';
    }

    public function output(): array|string
    {
        return $this->data;
    }
}