<?php

declare(strict_types=1);

namespace App\Bridge;

use App\CountryCounter\CountryCount;
use App\CountryCounter\CountryCountStorage;
use Predis\Client;

final class RedisCountryCountStorage implements CountryCountStorage
{
    public function __construct(
        private Client $client
    ) {}

    /**
     * @inheritDoc
     */
    public function getAll(): array
    {
        $data = $this->client->hgetall('countries') ?? [];
        $result = [];
        array_walk($data, function(string $value, string $key) use (&$result) {
            $result[] = new CountryCount($key, (int) $value);
        });
        return $result;
    }

    public function increase(CountryCount $count): void
    {
        $this->client->hincrby('countries', $count->countyCode, $count->count);
    }
}