<?php

declare(strict_types=1);

namespace App\CountryCounter\UseCase;

use App\CountryCounter\CountryCount;
use App\CountryCounter\CountryCountStorage;

final class IncreaseCountryCountHandler
{
    public function __construct(
        private CountryCountStorage $storage
    ) {}

    public function __invoke(IncreaseCountryCount $increaseCountryCount)
    {
        $this->storage->increase(new CountryCount($increaseCountryCount->countryCode, 1));
    }
}