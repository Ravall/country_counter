<?php

declare(strict_types=1);

namespace App\CountryCounter\UseCase;

/**
 * @psalm-immutable
 */
final class IncreaseCountryCount
{
    public string $countryCode;

    public function __construct(string $countryCode)
    {
        $this->countryCode = $countryCode;
    }
}