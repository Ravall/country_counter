<?php

declare(strict_types=1);

namespace App\CountryCounter;

interface CountryCountStorage
{
    /**
     * @return CountryCount[]
     */
    public function getAll(): array;

    public function increase(CountryCount $count): void;
}