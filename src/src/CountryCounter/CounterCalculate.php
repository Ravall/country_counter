<?php

declare(strict_types=1);

namespace App\CountryCounter;

class CounterCalculate
{
    public function __construct(
        private CountryCountStorage $storage
    ) {}

    /**
     * @return array<string, int>
     */
    public function getAllCountries(): array
    {
        $result = [];
        foreach ($this->storage->getAll()  as $countryCount) {
            $result[$countryCount->countyCode] = $countryCount->count;
        }

        return $result;
    }
}