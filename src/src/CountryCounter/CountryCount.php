<?php

declare(strict_types=1);

namespace App\CountryCounter;

/**
 * @psalm-imutable
 */
final class CountryCount
{
    public function __construct(
        public string $countyCode,
        public int $count
    ) {}
}