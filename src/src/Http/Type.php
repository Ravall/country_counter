<?php

declare(strict_types=1);

namespace App\Http;

interface Type
{
    /**
     * @return string
     */
    public function name(): string;
    /**
     * @return string|array
     */
    public function output(): array|string;
}