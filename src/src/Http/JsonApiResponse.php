<?php

declare(strict_types=1);

namespace App\Http;

use Symfony\Component\HttpFoundation\JsonResponse;

final class JsonApiResponse
{
    /**
     * @param Type ...$datum
     * @return JsonResponse
     */
    public static function success(Type ...$datum): JsonResponse
    {
        $dataResponse = [];
        foreach ($datum as $data) {
            $dataResponse[$data->name()] = $data->output();
        }

        $response = [
            'data' => $dataResponse,
            'successful' => true,
        ];

        return new JsonResponse($response, 200);
    }

}