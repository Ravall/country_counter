<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use Osteel\OpenApi\Testing\ValidatorBuilder;
use Osteel\OpenApi\Testing\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class ApiTest extends WebTestCase
{
    private ValidatorInterface $swaggerValidator;

    public function setUp(): void
    {
        parent::setUp();

        $this->swaggerValidator = ValidatorBuilder::fromYaml(__DIR__ . '/../../public/docs/swagger.yaml')
            ->getValidator();
    }

    /** @test  */
    public function getStat(): void
    {
        $client = static::createClient();

        $client->request('GET', '/countries/stat');

        $response = $client->getResponse();
        $this->assertResponseIsSuccessful();
        $content = $response->getContent();
        $json = json_decode($content, true);
        $this->assertTrue($json['successful']);

        $this->assertEquals([], $json['data']['statistic']);

        $this->swaggerValidator->validate($response, '/countries/stat', 'get');
    }

    /** @test  */
    public function increase(): void
    {
        $client = static::createClient();

        $client->request('POST', '/countries/ru/increase');

        $response = $client->getResponse();
        $this->assertResponseIsSuccessful();
        $content = $response->getContent();
        $json = json_decode($content, true);
        $this->assertTrue($json['successful']);

        $this->swaggerValidator->validate($response, '/countries/ru/increase', 'post');

        $client->request('GET', '/countries/stat');
        $response = $client->getResponse();
        $this->assertResponseIsSuccessful();
        $content = $response->getContent();
        $json = json_decode($content, true);
        $this->assertEquals(['ru' => 1], $json['data']['statistic']);

        $this->swaggerValidator->validate($response, '/countries/stat', 'get');
    }
}