<?php

declare(strict_types=1);

namespace App\Tests;

use App\CountryCounter\CountryCount;
use App\CountryCounter\CountryCountStorage;

final class TestCountryCountStorage implements CountryCountStorage
{
    static array $data = [];

    /**
     * @inheritDoc
     */
    public function getAll(): array
    {
        $result = [];
        foreach (self::$data as $countryCode => $count) {
            $result[] = new CountryCount($countryCode, $count);
        }
        return $result;
    }

    public function increase(CountryCount $count): void
    {
        self::$data[$count->countyCode] = (self::$data[$count->countyCode] ?? 0) +  $count->count;
    }
}