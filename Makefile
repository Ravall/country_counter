bash:
	docker-compose exec backend bash

down:
	docker-compose down

up:
	docker-compose up -d

build:
	docker-compose up -d --build